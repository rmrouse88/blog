#%%
import os

import numpy as np
import pandas as pd
from pandas import Series, DataFrame

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


os.chdir('C:/Users/rmrou/Documents/Northwestern/Predict 422/Predict422')

frame = pd.read_csv("charity.csv")

print(frame.head())
print(frame.tail())

#%%
sns.heatmap(frame.isnull(), xticklabels=False)